FROM node:12.7-alpine AS build

WORKDIR /usr/websocket-server
COPY package.json /usr/websocket-server

RUN npm install

COPY ./ /usr/websocket-server
RUN npm run build:websocket-server

COPY ./ /usr/websocket-server

EXPOSE 8999

CMD ["node", "dist/apps/websocket-server/main.js"]