FROM node:12.7-alpine AS build

ADD package.json /tmp/package.json
ADD package-lock.json /tmp/package-lock.json
RUN cd /tmp && npm install
RUN mkdir -p /app/telemetry-client && cp -a /tmp/node_modules /app/telemetry-client/

WORKDIR /app/telemetry-client
COPY dist/apps/telemetry-client/ /app/telemetry-client

CMD ["node", "main.js"]