import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NextEventContainerComponent } from './container/next-event-container/next-event-container.component';
import { NzCardModule } from 'ng-zorro-antd/card';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzPageHeaderModule } from 'ng-zorro-antd/page-header';

@NgModule({
  imports: [CommonModule,  NzPageHeaderModule, NzGridModule, NzCardModule],
  exports: [NextEventContainerComponent],
  declarations: [NextEventContainerComponent],
})
export class FeatureNextEventModule {}
