import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'f1-event-app-next-event-container',
  templateUrl: './next-event-container.component.html',
  styleUrls: ['./next-event-container.component.scss']
})
export class NextEventContainerComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
