import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NextEventContainerComponent } from './next-event-container.component';

describe('NextEventContainerComponent', () => {
  let component: NextEventContainerComponent;
  let fixture: ComponentFixture<NextEventContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NextEventContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NextEventContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
