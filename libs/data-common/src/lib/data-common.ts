export interface Participant {
  m_nationality:
    | 6
    | 15
    | 16
    | 19
    | 20
    | 22
    | 24
    | 29
    | 42
    | 51
    | 57
    | 58
    | 64
    | 65
    | 67
    | 71
    | 78
    | 80
    | 82;
}

export interface LapData {
  m_carPosition: number;
  m_sector1Time: number;
  m_sector2Time: number;
  m_lastLapTime: number;
  m_bestLapTime: number;
  m_penalties: number;
}

export interface RaceData extends LapData, Participant {}

export interface Options {
  port?: number;
}

