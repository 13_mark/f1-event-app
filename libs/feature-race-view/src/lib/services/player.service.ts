import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class PlayerService {
  constructor() {}

  getPlayerByNationNumber(nation: number) {
    switch (nation) {
      case 6:
        return 'Mark';
      case 15:
        return 'Max Groß';
      case 16:
        return 'Jonas';
      case 19:
        return 'Leo';
      case 20:
        return 'Moritz';
      case 22:
        return 'Benedikt';
      case 24:
        return 'Sabine';
      case 29:
        return 'Bastian';
      case 42:
        return 'Max Bielecke';
      case 51:
        return 'Jesse';
      case 57:
        return 'Florian';
      case 58:
        return 'Laurin';
      case 64:
        return 'Valentin';
      case 65:
        return 'Fabian';
      case 67:
        return 'Adrian';
      case 71:
        return 'Robin';
      case 78:
        return 'Georg';
      case 80:
        return 'Frank';
      case 82:
        return 'Gianluca';
      default:
        return 'Unknown Player';
    }
  }
}
