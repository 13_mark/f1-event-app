import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { RaceData } from '@f1-event-app/data-common';

@Injectable({
  providedIn: 'root'
})
export class RaceDataService {
  private readonly myWebSocket: WebSocket = new WebSocket('wss://f1-api.cratory.de');
  private readonly dataSubject = new Subject<RaceData[]>();

  constructor() {
    this.myWebSocket.addEventListener<'message'>('message', (val: any) => {
      const data: RaceData[] = JSON.parse(val.data);
      this.dataSubject.next(data);
    });
  }

  getCurrentStanding(): Observable<RaceData[]> {
    return this.dataSubject
      .pipe(
        map((data) => data.sort((a, b) => (a.m_carPosition < b.m_carPosition ? -1 : 1))),
      );
  }

  getPlayerWithFastestRound(): Observable<{nation: number, lapTime: number}> {
    return this.dataSubject
      .pipe(
        map((data) => data.reduce((a, b) => (a.m_bestLapTime < b.m_bestLapTime ? a : b))),
        map((raceData) => ({nation: raceData.m_nationality, lapTime: raceData.m_bestLapTime}))
      );
  }
}
