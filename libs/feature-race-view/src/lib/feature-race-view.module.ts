import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RaceViewContainerComponent } from './container/race-view-container/race-view-container.component';
import { RaceTableComponent } from './components/race-table/race-table.component';
import { FastestPlayerCardComponent } from './components/fastest-player-card/fastest-player-card.component';
import { NzCardModule } from 'ng-zorro-antd/card';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzDividerModule } from 'ng-zorro-antd/divider';
import { MinuteSecondPipe } from './pipes/minute-second.pipe';
import { RaceDataService } from './services/race-data.service';

@NgModule({
  imports: [CommonModule, NzCardModule, NzTableModule, NzDividerModule],
  exports: [RaceViewContainerComponent],
  providers: [RaceDataService],
  declarations: [RaceViewContainerComponent, RaceTableComponent, FastestPlayerCardComponent, MinuteSecondPipe],
})
export class FeatureRaceViewModule {
}
