import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'f1-event-app-race-view-container',
  templateUrl: './race-view-container.component.html',
  styleUrls: ['./race-view-container.component.scss']
})
export class RaceViewContainerComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
