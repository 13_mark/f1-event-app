import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RaceViewContainerComponent } from './race-view-container.component';

describe('RaceViewContainerComponent', () => {
  let component: RaceViewContainerComponent;
  let fixture: ComponentFixture<RaceViewContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RaceViewContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RaceViewContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
