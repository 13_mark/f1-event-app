import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'minuteSecond'
})
export class MinuteSecondPipe implements PipeTransform {

  transform(seconds: number): unknown {
    if (seconds > 0) {
      const minutes: number = Math.floor(seconds / 60);
    const minutesWithLeadingZero = minutes.toString().padStart(2, '0');
    const secondsWithLeadingZero = Math.trunc(seconds - minutes * 60)
      .toString()
      .padStart(2, '0');
    const millisWithLeadingZero = (
      seconds -
      minutes * 60 -
      Math.trunc(seconds - minutes * 60)
    )
      .toString()
      .split(".")[1]
      .padStart(4, '0')
      .substring(0, 4);
    return `${minutesWithLeadingZero}:${secondsWithLeadingZero}.${millisWithLeadingZero}`;
    }

    return '00:00.00';
    
  }

}
