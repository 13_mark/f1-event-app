import { Component, OnInit } from '@angular/core';
import { RaceData } from '@f1-event-app/data-common';
import { FlagService } from '../../services/flag.service';
import { PlayerService } from '../../services/player.service';
import { Observable } from 'rxjs';
import { RaceDataService } from '../../services/race-data.service';

@Component({
  selector: 'f1-event-app-race-table',
  templateUrl: './race-table.component.html',
  styleUrls: ['./race-table.component.scss'],
})
export class RaceTableComponent implements OnInit {
  raceData: RaceData[] = [];
  fastestPlayer: Observable<{ nation: number; lapTime: number }>;

  constructor(
    private readonly flagService: FlagService,
    private readonly playerService: PlayerService,
    private readonly raceDataService: RaceDataService
  ) {
    this.raceDataService.getCurrentStanding().subscribe(
      (next) => this.raceData = next
    );
  }

  ngOnInit(): void {
  }

  getPlayerName(nation: number): string {
    return this.playerService.getPlayerByNationNumber(nation);
  }

  getClass(nation: number): string {
    return `flag-icon flag-icon-${this.flagService.getFlagByNationNumber(
      nation
    )} margin-right`;
  }
}
