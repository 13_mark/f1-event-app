import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FastestPlayerCardComponent } from './fastest-player-card.component';

describe('FastestPlayerCardComponent', () => {
  let component: FastestPlayerCardComponent;
  let fixture: ComponentFixture<FastestPlayerCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FastestPlayerCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FastestPlayerCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
