import { Component, OnInit } from '@angular/core';
import { RaceDataService } from '../../services/race-data.service';
import { PlayerService } from '../../services/player.service';

@Component({
  selector: 'f1-event-app-fastest-player-card',
  templateUrl: './fastest-player-card.component.html',
  styleUrls: ['./fastest-player-card.component.scss']
})
export class FastestPlayerCardComponent implements OnInit {

  fastestPlayer: {nation: number, lapTime: number} = {lapTime: 0, nation: 100}

  constructor(private readonly raceDataService: RaceDataService, private readonly playerService: PlayerService) { }

  ngOnInit(): void {
    this.raceDataService.getPlayerWithFastestRound().subscribe(
      (next) => this.fastestPlayer = next 
    );
    
  }

  getPlayerName(nation: number): string {
    return this.playerService.getPlayerByNationNumber(nation);
  }

}
