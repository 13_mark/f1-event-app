import { Component } from '@angular/core';
import { RaceDataService } from 'libs/feature-race-view/src/lib/services/race-data.service';


@Component({
  selector: 'f1-event-app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  isEventRunning = false;

  constructor(private readonly raceDataService: RaceDataService) {
    this.raceDataService.getCurrentStanding().subscribe(
      (next) => {
        if (next.length !== 0) {
          this.isEventRunning = true;
        }
      }
    )
  }
}
