import axios from 'axios';
import { Participant, LapData, RaceData } from '@f1-event-app/data-common';
import { Subject } from 'rxjs';
import { throttleTime } from 'rxjs/operators';
import { F1TelemetryClient, constants } from "f1-telemetry-client";
const { PACKETS } = constants;
const https = require('https');

const client = new F1TelemetryClient({ port: 20777 });
const dataSubject = new Subject();
const filtered = dataSubject.pipe(throttleTime(5000));

function mapToRaceData(participants: Participant[], lapData: LapData[]): RaceData[] {
  const data = [];
  for (let i = 0; i < participants.length; i++) {
    data.push({ ...lapData[i], ...participants[i] });
  }
  return data;
}

client.on(PACKETS.participants, (participants: { m_participants: Participant[] }) => {
  client.on(PACKETS.lapData, async (lapData: { m_lapData: LapData[] }) => {
    const data = mapToRaceData(participants.m_participants, lapData.m_lapData);
    dataSubject.next(data);
  });
});

const subscribtion = filtered.subscribe(async (next) => {
  const agent = new https.Agent({
    rejectUnauthorized: false,
  });
  await axios.post('https://f1-api.cratory.de/data', next, { httpsAgent: agent });
});

client.start();

[`exit`, `SIGINT`, `SIGUSR1`, `SIGUSR2`, `uncaughtException`, `SIGTERM`].forEach(
  (eventType) => {
    (process as NodeJS.EventEmitter).on(eventType, () => {
      subscribtion.unsubscribe();
      console.log('client stopped by', eventType);
      client.stop();
      client.removeAllListeners();
    });
  },
);
