import express from 'express';
import * as http from 'http';
import * as WebSocket from 'ws';
import bodyParser from 'body-parser';
import { RaceData } from '@f1-event-app/data-common';

const router = express.Router();
const app = express();

// TODO: remove deprecated bodyparser
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

router.post('/data', function (req: express.Request, res: express.Response) {
  sendSomething(req.body);
  res.end(req.body);
});

app.use('/', router);

const server = http.createServer(app);

const wss = new WebSocket.Server({ server });

wss.on('connection', (ws: WebSocket) => {
  ws.on('message', (message: string) => {
    console.log('received: %s', message);
  });
});

server.listen(process.env.PORT || 8999, () => {
  console.log(`Server started on port: ${process.env.PORT || 8999})`);
});

function sendSomething(value: RaceData) {
  wss.clients.forEach((client: WebSocket) => {
    client.send(JSON.stringify(value));
  });
}
